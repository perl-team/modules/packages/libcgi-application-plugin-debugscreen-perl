package DebugScreenApp;
use strict;
use warnings;
use base qw(CGI::Application);
use CGI::Application::Plugin::ViewCode;
use CGI::Application::Plugin::DebugScreen;

sub setup {
    my $self = shift;
    $self->start_mode('start');
    $self->run_modes(start=>'start',die=>'die_abort_finito');
    return;
}

sub start {
    my $self = shift;
    return "Hello world!";
}

sub die_abort_finito {
    my $self = shift;
    die "Is this really it?";
}

1;

=head1 DESCRIPTION

This CGI script is only a test example. It provides the following run
modes:

=over

=item start - Returns a simple "Hello world" string.

=item view_code - As provided by L<CGI::Application::Plugin::ViewCode>.

=item view_pod  - As provided by L<CGI::Application::Plugin::ViewCode>.

=item die - Provided so that one can test
L<CGI::Application::Plugin::DebugScreen>.

